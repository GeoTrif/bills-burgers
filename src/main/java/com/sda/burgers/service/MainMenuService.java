package com.sda.burgers.service;

import com.sda.burgers.util.Menu;
import com.sda.burgers.util.ScannerUtil;

public class MainMenuService {

	private Menu menu = new Menu();
	private ScannerUtil scannerUtil = new ScannerUtil();
	private boolean flag;

	public void defineMainMenuFunctionality() {
		while (!flag) {
			menu.printMainMenu();
			System.out.println("Enter your choice:");
			int choice = scannerUtil.intScanner();
			selectMainMenuChoice(choice);
		}
	}

	private void selectMainMenuChoice(int choice) {
		switch (choice) {
		case 1:
			BurgerMenuService.defineBurgerMenuFunctionality();
			break;

		case 2:
			System.out.println("Exiting application...");
			flag = true;
			break;

		default:
			System.out.println("Incorrect choice.");
			defineMainMenuFunctionality();
		}
	}
}
