package com.sda.burgers.service;

import com.sda.burgers.util.Menu;
import com.sda.burgers.util.ScannerUtil;

public class BurgerMenuService {

	private static Menu menu = new Menu();
	private static ScannerUtil scannerUtil = new ScannerUtil();
	private static MainMenuService mainMenuService = new MainMenuService();
	private static HamburgerService hamburgerService = new HamburgerService();
	private static HealthyBurgerService healthyBurgerService = new HealthyBurgerService();
	private static DeluxeBurgerService deluxeBurgerService = new DeluxeBurgerService();

	public static void defineBurgerMenuFunctionality() {
		while (true) {
			menu.printBurgerMenu();
			System.out.println("Enter your choice:");
			int choice = scannerUtil.intScanner();
			selectBurgerMenuChoice(choice);
		}
	}

	private static void selectBurgerMenuChoice(int choice) {
		switch (choice) {
		case 1:
			hamburgerService.defineHamburgerMenuFunctionality();
			break;

		case 2:
			healthyBurgerService.defineHealthyBurgerMenuFunctionality();
			break;

		case 3:
			deluxeBurgerService.defineDeluxeBurgerMenuFunctionality();
			break;

		case 4:
			mainMenuService.defineMainMenuFunctionality();
			break;

		case 5:
			System.out.println("Exiting application...");
			System.exit(0);

		default:
			System.out.println("Incorrect choice.");
			defineBurgerMenuFunctionality();
		}
	}

}
