package com.sda.burgers.service;

import com.sda.burgers.model.DeluxeBurger;
import com.sda.burgers.util.BurgerCreatorUtil;
import com.sda.burgers.util.Menu;
import com.sda.burgers.util.ScannerUtil;

public class DeluxeBurgerService {

	private static final String ENTER_YOUR_CHOICE_STRING = "Enter your choice:";

	private DeluxeBurger deluxeBurger = new DeluxeBurger();
	private Menu menu = new Menu();
	private ScannerUtil scannerUtil = new ScannerUtil();

	public void defineDeluxeBurgerMenuFunctionality() {
		while (true) {
			menu.printDeluxeBurgerMenu();
			System.out.println("Enter your choice:");
			int choice = scannerUtil.intScanner();
			selectDeluxeBurgerChoice(choice);
		}
	}

	private void selectDeluxeBurgerChoice(int choice) {
		switch (choice) {

		case 1:
			menu.printChipsMenu();
			System.out.println(ENTER_YOUR_CHOICE_STRING);
			int chipsChoice = scannerUtil.intScanner();
			BurgerCreatorUtil.addChips(chipsChoice, deluxeBurger);
			break;

		case 2:
			menu.printDrinkMenu();
			System.out.println(ENTER_YOUR_CHOICE_STRING);
			int drinkChoice = scannerUtil.intScanner();
			BurgerCreatorUtil.addDrink(drinkChoice, deluxeBurger);
			break;

		case 3:
			System.out.println("Your burger has the following items:");
			System.out.println("Chips -> " + deluxeBurger.getChips());
			System.out.println("Drink -> " + deluxeBurger.getDrink());
			System.out.println("Total price of the burger: " + deluxeBurger.getPrice() + "$.");
			break;

		case 4:
			System.out.println("Order sended.Yor order is priced at " + deluxeBurger.getPrice() + "$.");
			break;

		case 5:
			BurgerMenuService.defineBurgerMenuFunctionality();
			break;

		case 6:
			System.out.println("Exiting application...");
			System.exit(0);

		default:
			System.out.println("Incorrect choice.");
		}
	}
}
