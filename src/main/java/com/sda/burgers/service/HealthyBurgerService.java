package com.sda.burgers.service;

import java.util.Map;

import com.sda.burgers.model.HealthyBurger;
import com.sda.burgers.util.BurgerCreatorUtil;
import com.sda.burgers.util.Menu;
import com.sda.burgers.util.ScannerUtil;

public class HealthyBurgerService {

	private static final int MAXIMUM_ADDITIONS_PER_HEALTHY_BURGER = 6;
	private static final String ENTER_YOUR_CHOICE_STRING = "Enter your choice:";

	private HealthyBurger healthyBurger = new HealthyBurger();
	private Menu menu = new Menu();
	private ScannerUtil scannerUtil = new ScannerUtil();
	private Map<String, Double> selectedAdditions = BurgerCreatorUtil.selectedAdditions;

	public void defineHealthyBurgerMenuFunctionality() {
		while (true) {
			menu.printHealthyBurgerMenu();
			System.out.println("Enter your choice:");
			int choice = scannerUtil.intScanner();
			healthyBurger.setBreadRollType("Brown Rye Bread Roll");
			selectHealthyBurgerChoice(choice);
		}
	}

	private void selectHealthyBurgerChoice(int choice) {
		switch (choice) {
		case 1:
			menu.printMeatMenu();
			System.out.println(ENTER_YOUR_CHOICE_STRING);
			int meatChoice = scannerUtil.intScanner();
			BurgerCreatorUtil.addMeatPatty(meatChoice, healthyBurger);
			break;

		case 2:
			menu.printAdditionsMenu();
			System.out.println(ENTER_YOUR_CHOICE_STRING);
			int additionsChoice = scannerUtil.intScanner();
			if (BurgerCreatorUtil.additionsCounter < MAXIMUM_ADDITIONS_PER_HEALTHY_BURGER) {
				BurgerCreatorUtil.addAdditions(additionsChoice, healthyBurger);
			} else {
				System.out.println("Maximum 6 additions.You can't have more.");
			}
			break;

		case 3:
			System.out.println("Additions pricing");
			Map<String, Double> additions = healthyBurger.defineAdditions();
			for (Map.Entry<String, Double> addition : additions.entrySet()) {
				System.out.println(addition.getKey() + " -> " + addition.getValue());
			}
			break;

		case 4:
			System.out.println("Your burger has the following items:");
			System.out.println("Bread Roll -> " + healthyBurger.getBreadRollType());
			System.out.println("Meat -> " + healthyBurger.getMeat());
			System.out.println("Additions -> " + selectedAdditions.size() + " additions selected; "
					+ (MAXIMUM_ADDITIONS_PER_HEALTHY_BURGER - selectedAdditions.size()) + " additions available.");
			for (Map.Entry<String, Double> addition : selectedAdditions.entrySet()) {
				System.out.println(addition.getKey() + " -> " + addition.getValue());
			}
			System.out.println("Total price of the burger: " + healthyBurger.getPrice() + "$.");
			break;

		case 5:
			System.out.println("Order sended.Yor order is priced at " + healthyBurger.getPrice() + "$.");
			break;

		case 6:
			BurgerMenuService.defineBurgerMenuFunctionality();
			break;

		case 7:
			System.out.println("Exiting application...");
			System.exit(0);

		default:
			System.out.println("Incorrect choice.");
		}

	}

}
