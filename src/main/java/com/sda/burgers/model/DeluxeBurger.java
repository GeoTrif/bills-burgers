package com.sda.burgers.model;

public class DeluxeBurger {

	private String chips;
	private String drink;
	private double price = 20;

	public DeluxeBurger() {

	}

	public DeluxeBurger(String chips, String drink, double price) {
		super();
		this.chips = chips;
		this.drink = drink;
	}

	public String getChips() {
		return chips;
	}

	public void setChips(String chips) {
		this.chips = chips;
	}

	public String getDrink() {
		return drink;
	}

	public void setDrink(String drink) {
		this.drink = drink;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
