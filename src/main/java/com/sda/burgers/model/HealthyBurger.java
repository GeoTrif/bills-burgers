package com.sda.burgers.model;

public class HealthyBurger extends Hamburger {

	public HealthyBurger() {

	}

	public HealthyBurger(String breadRollType, String meat) {
		super(breadRollType, meat);
	}
}
