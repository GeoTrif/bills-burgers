package com.sda.burgers.util;

import java.util.Scanner;

public class ScannerUtil {

	private Scanner scanner = new Scanner(System.in);

	public int intScanner() {
		return scanner.nextInt();
	}

	public double doubleScanner() {
		return scanner.nextDouble();
	}

	public String stringScanner() {
		return scanner.next();
	}
}
